package com.natours.natoursbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NatoursBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(NatoursBackendApplication.class, args);
	}

}
